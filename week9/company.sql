select*from customers;
update customers set country=replace(country, '\n', '');
update customers set city=replace(city, '\n', '');

create view mexicancustomers as
select customerid, customername, contactname
from customers
where country="Mexico";

select*from mexicancustomers;

select*
from mexicancustomers join orders on mexicancustomers.customerid=orders.customer;

create view productsbelowang as
select productid, productname, price
from products
where price < (select avg(price) from products);

delete from orderdetails where ProductID=5;
truncate orderdetails;

delete from customers;
delete from orders;

drop table customers;
